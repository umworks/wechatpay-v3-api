<?php


use Um\WechatPay\PayV3Request;
use PHPUnit\Framework\TestCase;

class PayV3RequestTest extends TestCase
{

	use PayV3TestTrait;

	public function testSetPrivateKeyFile()
	{
		$req = $this->preRequest('https://api.mch.weixin.qq.com/v3/certificates');

		$this->assertTrue($req->hasPrivateKey());
	}

	public function testCanonicalUrl()
	{
		$req = $this->preRequest('https://api.mch.weixin.qq.com/v3/certificates?hello=world');

		$this->assertEquals($req->getCanonicalUrl(), '/v3/certificates?hello=world');
		$this->assertEquals($req->getMethodUpper(), 'GET');
	}

	public function testSend1()
	{
		$req = $this->preRequest('https://api.mch.weixin.qq.com/v3/certificates');

		$req->get();
		$resp = $req->getResponseAsJson();
		var_export($resp);
	}

	public function testSend2()
	{
		$url = 'https://api.mch.weixin.qq.com/v3/marketing/favor/stocks';
		$query = "?offset=0&limit=10&stock_creator_mchid={$this->getMerchantId()}&status=running";
		$req = $this->preRequest("{$url}{$query}");

		$req->send();
		$resp = $req->getResponseAsJson();
		var_export($resp);

	}

	public function testSendError()
	{
		// 以下为微信商户的 Postman 的测试用例的数据，不涉及暴露隐私内容
		// https://github.com/wechatpay-apiv3/wechatpay-postman-script/blob/master/wechatpay-apiv3.postman_collection.json
		$url = 'https://api.mch.weixin.qq.com/v3/marketing/favor/users/oHkLxt_htg84TUEbzvlMwQzVDBqo/coupons';
		$data = [
			'stock_id'            => '9433645',
			'stock_creator_mchid' => '1900006511',
			'out_request_no'      => '20190522_001中文11',
			'appid'               => 'wxab8acb865bb1637e',
		];
		$req = $this->preRequest($url);
		$req->post($data);

		var_export($req->getResponseAsJson());
	}
}
