<?php


namespace Um\WechatPay;


use Ke\Utils\Status;

/**
 * Class PayStatus
 *
 * @see     https://wechatpay-api.gitbook.io/wechatpay-api-v3/wei-xin-zhi-fu-api-v3-jie-kou-gui-fan#cuo-wu-ma-he-cuo-wu-ti-shi
 * @package Um\WechatPay
 */
class PayV3Status extends Status
{

	private $code = '';

	public function __construct(array $respData)
	{
		$this->code = $respData['code'] ?? '';
		$message = $respData['message'] ?? '';
		if (!empty($this->code))
			parent::__construct(false, $message, $respData['detail'] ?? []);
		else
			parent::__construct(true, $message, $respData ?? []);
	}

	/**
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}
}