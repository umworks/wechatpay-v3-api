<?php


use Um\WechatPay\PayV3Api;
use Um\WechatPay\V3Api\VoucherApi;
use PHPUnit\Framework\TestCase;

class VoucherApiTest extends TestCase
{

	use PayV3TestTrait;

	/**
	 * @var PayV3Api
	 */
	private $api;

	/**
	 * @var VoucherApi
	 */
	private $voucherApi;

	protected function setUp(): void
	{
		$this->api = PayV3Api::getInstance('test');
		$this->api->init($this->getMerchantId(), $this->getMerchantSerialNo(), $this->getPrivateKeyFile());
		$this->voucherApi = $this->api->getVoucherApi();
	}

	public function testGetVouchers()
	{
		$st = $this->voucherApi->getVouchers();

		$this->assertTrue($st->isSuccess());
		$this->assertEquals($st->getCode(), '');
		// 这个测试可能在不同的环境会有所不同
//		$this->assertTrue(count($st->data) > 0);
		var_dump($st);
	}

	public function testGetVoucher()
	{
		foreach ($this->loadVouchers() as $voucherData) {
			$stockId = $voucherData['stock_id'] ?? '';
			$st = $this->voucherApi->getVoucher($stockId);
			var_export($st);
			if ($st->isSuccess()) {
				$this->assertTrue($st->isSuccess());
				$this->assertEquals($st->data['stock_id'], $stockId);
			}
		}
	}

	public function testGiveVoucher()
	{
		$user = $this->loadUser();
		$openId = $user['open_id'] ?? '';
		$merchantConfig = $this->loadMerchantConfig();
		$merchantAppId = $merchantConfig['appId'] ?? '';

		foreach ($this->loadVouchers() as $voucherData) {
			$stockId = $voucherData['stock_id'] ?? '';
			$outRequestNo = $voucherData['out_request_no'] ?? '';
			$st = $this->voucherApi->giveVoucher($stockId, $openId, $merchantAppId, $outRequestNo);
			$this->assertTrue($st->isSuccess());
			var_dump($st);
		}
	}

	public function testGetUserVoucher()
	{
		$user = $this->loadUser();
		$openId = $user['open_id'] ?? '';
		$couponIds = $user['coupon_ids'] ?? [];
		$merchantConfig = $this->loadMerchantConfig();
		$merchantAppId = $merchantConfig['appId'] ?? '';

		foreach ($couponIds as $couponId) {
			$st = $this->voucherApi->getUserVoucher($couponId, $openId, $merchantAppId);
			$this->assertTrue($st->isSuccess());
			$this->assertEquals($st->data['coupon_id'], $couponId);
			var_dump($st);
		}
	}

	public function testGetUserVouchers()
	{
		$user = $this->loadUser();
		$openId = $user['open_id'] ?? '';
		$merchantConfig = $this->loadMerchantConfig();
		$merchantAppId = $merchantConfig['appId'] ?? '';
		$st = $this->voucherApi->getUserVouchers($openId, $merchantAppId, null, null, 0);
//		$this->assertTrue($st->isSuccess());
//			$this->assertEquals($st->data['coupon_id'], $couponId);
		var_export($st->data);

	}

	public function testGetVoucherMerchants()
	{
		foreach ($this->loadVouchers() as $voucherData) {
			$stockId = $voucherData['stock_id'] ?? '';
			$st = $this->voucherApi->getVoucherMerchants($stockId);
			var_dump($st);
		}
	}

	public function testStartVoucher()
	{
		foreach ($this->loadVouchers() as $voucherData) {
			$stockId = $voucherData['stock_id'] ?? '';
			$st = $this->voucherApi->startVoucher($stockId);
			var_export($st);
//			break;
		}
	}

	public function testCreateVoucher()
	{

		$beginTime = date('c', strtotime('+1 hours'));
		$endTime = date('c', strtotime('+1 months'));

		$stockUseRule = [
			'max_coupons'          => 100,
			'max_amount'           => 10000,
			'max_amount_by_day'    => 100,
			'max_coupons_per_user' => 10,
			'natural_person_limit' => false,
			'prevent_api_abuse'    => false,
		];
		$patternInfo = [
			'description'      => '接口添加代金券二',
			//			'merchant_logo' => '',
			'merchant_name'    => 'k11',
			'background_color' => 'Color020',
			//			'coupon_image' => '',
		];
		$couponUseRule = [
			'coupon_available_time' => [
				'fix_available_time' => [
					"begin_time" => 0,
					"end_time"   => 3600,
					//					'available_week_day' => 1,
				],
				//				'second_day_available' => false,
				//				'available_time_after_receive' => 0,
			],
			'fixed_normal_coupon'   => [
				'coupon_amount'       => 100,
				'transaction_minimum' => 100,
			],
			'disscount_coupon'      => [
				'discount_amount_max' => 100,
				'discount_percent'    => 100,
				//				'transaction_minimum' => 100,
			],
			'exchange_coupon'       => [
				'single_price_max' => 100,
				'exchange_price'   => 100,
			],
			//			'goods_tag'      => '',
			//			'limit_pay'      => '',
			//			'limit_card'      => '',
			//			'trade_type'      => '',
			'combine_use'           => false,
			//			'available_items'      => '',
			//			'unavailable_items'      => '',
			'available_merchants'   => [$this->getMerchantId()],
		];

		$outRequestNo = $this->getMerchantId() . date('Ymd') . mt_rand(1000000, 9999999);

		$data = [
			'stock_name'           => '微信支付代金批次二',
			'belong_merchant'      => $this->getMerchantId(),
			'available_begin_time' => $beginTime,
			'available_end_time'   => $endTime,
			'stock_use_rule'       => $stockUseRule,
			'pattern_info'         => $patternInfo,
			'coupon_use_rule'      => $couponUseRule,
			'no_cash'              => true,
			'stock_type'           => 'NORMAL',
			'out_request_no'       => $outRequestNo,
			//			'ext_info'             => '',
		];
		$st = $this->voucherApi->createVoucher($data);
		var_dump($outRequestNo);
		var_dump($st);
	}

	public function testPauseVoucher()
	{
		foreach ($this->loadVouchers() as $voucherData) {
			$stockId = $voucherData['stock_id'] ?? '';
			$st = $this->voucherApi->pauseVoucher($stockId);
			var_export($st);
//			break;
		}
	}

	public function testRestartVoucher()
	{
		foreach ($this->loadVouchers() as $voucherData) {
			$stockId = $voucherData['stock_id'] ?? '';
			$st = $this->voucherApi->restartVoucher($stockId);
			var_export($st);
//			break;
		}
	}
}
