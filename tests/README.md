# 测试说明

需要将 example.protected 复制为 protected

或者在 混入了 PayV3TestTrait 以后自行设置 `$this->setDataDir()` 数据目录。

数据目录需要确保满足以下内容：

- merchant_config.php 输出商户号和商户 serialNo
    - 有些接口（如：getUserVouchers）需要用到公众号appId，和appKey，则需要配置此参数
- apiclient_key.pem 私钥
- 测试商户代金券，需要自行填充 vouchers.php 内的数据

