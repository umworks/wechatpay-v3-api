<?php

use Um\WechatPay\PayV3Request;

/**
 * Trait PayV3TestTrait
 * @see README.md file please
 */
trait PayV3TestTrait
{

	private $dataDir = '/protected/';

	private $vouchers = null;

	private $loadMerchantConfig = false;

	private $merchantConfig = [
		'merchantId' => '',
		'serialNo'   => '',
	];

	public function loadVouchers()
	{
		if ($this->vouchers === null) {
			$vouchers = require($this->dataFileOf('vouchers.php', true));
			if (!is_array($vouchers))
				$vouchers = [];
			$this->vouchers = $vouchers;
		}
		return $this->vouchers;
	}

	public function loadMerchantConfig()
	{
		if ($this->loadMerchantConfig === false) {
			$data = require($this->dataFileOf('merchant_config.php', true));
			if (is_array($data) && !empty($data)) {
				$this->merchantConfig = array_merge($this->merchantConfig, $data);
			}
		}
		return $this->merchantConfig;
	}

	public function getPrivateKeyFile()
	{
		return $this->dataFileOf('apiclient_key.pem', true);
	}

	public function loadUser()
	{
		$user = require($this->dataFileOf('user.php', true));
		if (!is_array($user))
			$user = [];
		return $user;
	}

	public function setDataDir(string $dir)
	{
		$dir = trim($dir, '\\\/ ');
		if (empty($dir))
			throw new Exception('Data dir cannot be empty!');
		if (strpos($dir, '\\'))
			$dir = str_replace('\\', '/', $dir);
		$this->dataDir = "/{$dir}/";
		return $this;
	}

	public function dataFileOf($file, $isFileCheck = false)
	{
		$file = __DIR__ . $this->dataDir . ltrim($file, '\\\/ ');
		if ($isFileCheck && !is_file($file))
			throw new Exception("$file 不是有效的文件");
		return $file;
	}

	public function getMerchantId()
	{
		if (!$this->loadMerchantConfig) $this->loadMerchantConfig();
		return $this->merchantConfig['merchantId'];
	}

	public function getMerchantSerialNo()
	{
		if (!$this->loadMerchantConfig) $this->loadMerchantConfig();
		return $this->merchantConfig['serialNo'];
	}

	public function getMerchantAppId()
	{
		if (!$this->loadMerchantConfig) $this->loadMerchantConfig();
		return $this->merchantConfig['appId'];
	}

	/**
	 * @param $url
	 * @return PayV3Request
	 * @throws \Um\WechatPay\PayException|Exception
	 */
	public function preRequest($url)
	{
		if (!$this->loadMerchantConfig)
			$this->loadMerchantConfig();
		$req = new PayV3Request($url);
		if (empty($this->merchantConfig))
			throw new Exception('商户配置信息为空！');
		if (empty($this->merchantConfig['merchantId']) || empty($this->merchantConfig['serialNo']))
			throw new Exception('商户配置信息缺失字段！');

		$req->setPrivateKeyFile($this->getPrivateKeyFile());
		$req->setMerchantId($this->merchantConfig['merchantId']);
		$req->setSerialNo($this->merchantConfig['serialNo']);
		return $req;
	}
}