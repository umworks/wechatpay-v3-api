# wechatpay-v3-api

## 介绍

微信商户的 v3 接口封装。

单元测试请参考 [测试说明](tests/README.md) 。

**请严格遵守以下的准则来进行开发和测试**

- **禁止** 在 src的源代码 和 单元测试 的代码里，直接 hard code 任何商户ID、SerialNo、PrivateKey（文本形态，我知道很多程序员这么干） 等关键数据。
- **禁止** 在 src的源代码 和 单元测试 的代码里，直接写任意的卡券ID，ticket 等明文数据
- 所需的测试数据，**必须** 放在 `protected` 或 `private` 目录中（已经放入 .gitignore ），以文件的方式存放，并请遵守以下的守则：
    - 每在 `protected` 中添加一个所需的数据文件，必须在 `example.protected` 添加对应的文件
    - 每次修改、删除 `protected` 文件中的数据内容和字段，也必须在 `example.protected` 进行对应的操作
    - 每次提交前，请清除 `example.protected` 里的所有数据内容
- 不强制要求代码注释和文档，但每一个封装过的接口，**必须** 附上接口对应的微信商户的文档的地址，格式为：`// @see url...`

## 已实现特性和接口

公共特性

- V3 接口请求校验模式
- V3 接口响应内容区分成功与失败，并提取数据

接口部分

- 代金券 （VoucherApi）
    - 查询商户现有全部代金券（getVouchers）
    - 根据 stockId 查询商户特定代金券（getVoucher）
    - 发放的支付代金券 （giveVoucher）
        - 注意：该接口只是“发放”接口，并非把券放入到用户的卡包中。
        - 放入卡包的API接口，在H5页面中实现。（具体调用还需要确认）
    - 查询用户在某商户号可用的全部券（getUserVouchers）
    - 根据 couponId 查询用户特定代金券详情（getUserVoucher）
    - 根据 stockId 查询代金券可用的商户商户列表（getVoucherMerchants）

V2 接口

- 签名算法，调用如下：
    ```
  $api = PayV3Api::getInstance($name);
  $api->init( $merchantId,  $serialNo,  $privateKeyFile);
  
  $data = []; //需要签名的数据
  $appId = ''; //微信公众号的AppId
  $res = $api->getMpSendCouponSign($appId, $data);
    ```


## todo list 

- 校验 response 返回的 header status [接口基本规则](https://wechatpay-api.gitbook.io/wechatpay-api-v3/wei-xin-zhi-fu-api-v3-jie-kou-gui-fan) [签名验证](https://wechatpay-api.gitbook.io/wechatpay-api-v3/qian-ming-zhi-nan-1/qian-ming-yan-zheng)


