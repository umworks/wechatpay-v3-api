<?php


use Um\WechatPay\PayV3Api;
use PHPUnit\Framework\TestCase;

class PayV3ApiTest extends TestCase
{

	use PayV3TestTrait;

	/**
	 * @var PayV3Api
	 */
	private $api;

	protected function setUp(): void
	{
		$this->api = PayV3Api::getInstance('test');
		$this->api->init($this->getMerchantId(), $this->getMerchantSerialNo());
		$this->api->setPrivateKey(file_get_contents($this->getPrivateKeyFile()));
	}

	public function testFilterResponseInFailStatus()
	{
		// 以下为微信商户的 Postman 的测试用例的数据，不涉及暴露隐私内容
		// https://github.com/wechatpay-apiv3/wechatpay-postman-script/blob/master/wechatpay-apiv3.postman_collection.json
		$url = 'https://api.mch.weixin.qq.com/v3/marketing/favor/users/oHkLxt_htg84TUEbzvlMwQzVDBqo/coupons';
		$data = [
			'stock_id'            => '9433645',
			'stock_creator_mchid' => '1900006511',
			'out_request_no'      => '20190522_001中文11',
			'appid'               => 'wxab8acb865bb1637e',
		];
		$req = $this->api->newRequest($url);
		$req->post($data);

		$st = $this->api->filterResponse($req);

		$this->assertTrue($st->isFailure());
		$this->assertEquals($st->getCode(), 'PARAM_ERROR');
		var_export($st);
	}

	public function testFilterResponseInSuccess()
	{
		$url = 'https://api.mch.weixin.qq.com/v3/marketing/favor/stocks';
		$data = [
			'offset'              => 0,
			'limit'               => 10,
			'stock_creator_mchid' => $this->api->getMerchantId(),
			'status'              => 'running',
		];
		$req = $this->api->newRequest($url);
		$req->get($data);

		$st = $this->api->filterResponse($req);
		$this->assertTrue($st->isSuccess());
		$this->assertEquals($st->getCode(), '');
		var_export($st);
	}

	public function testGetMpSendCouponSign()
	{
		$data = [
			'appid'              => 'wxd930ea5d5a258f4f',
			'mch_id'             => 10000100,
			'device_info'        => 1000,
			'body'               => 'test',
			'nonce_str'          => 'ibuaiVcKdpRxkhJA',
			'send_coupon_params' => [
				[
					"stock_id"       => "98065001",
					"out_request_no" => "89560002019101000121",
				],
				[
					"stock_id"       => "98065001",
					"out_request_no" => "89560002019101000122",
				],
			],
		];
		$merchantConfig = $this->loadMerchantConfig();
		$appKey = $merchantConfig['appKey'] ?? '';
		$res = $this->api->getMpSendCouponSign($appKey, $data);
		var_export($res);
	}
}
