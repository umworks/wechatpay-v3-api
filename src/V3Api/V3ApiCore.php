<?php


namespace Um\WechatPay\V3Api;

use Um\WechatPay\PayException;
use Um\WechatPay\PayV3Api;
use Um\WechatPay\PayV3Request;
use Um\WechatPay\PayV3Status;

abstract
class V3ApiCore
{

	/** @var PayV3Api */
	protected $masterApi;

	/** @var PayV3Request */
	protected $lastRequest;

	public function __construct(PayV3Api $masterApi)
	{
		if (!isset($masterApi))
			throw new PayException('无效的主API');
		$this->masterApi = $masterApi;
	}

	/**
	 * @return PayV3Api
	 */
	public function getMasterApi()
	{
		return $this->masterApi;
	}

	/**
	 * @return PayV3Request
	 */
	public function getLastRequest()
	{
		return $this->masterApi->getLastRequest();
	}

	/**
	 * @param $url
	 * @return PayV3Request
	 * @throws PayException
	 */
	public function newRequest($url)
	{
		return $this->masterApi->newRequest($url);
	}

	public function filterResponse(PayV3Request $request)
	{
		$status = $this->masterApi->filterResponse($request);
		$newStatus = $this->onResponse($status, $request);
		if (!($newStatus instanceof PayV3Status))
			throw new \Exception('无效的接口返回状态');
		return $newStatus;
	}

	protected function onResponse(PayV3Status $status, PayV3Request $request)
	{
		return $status;
	}
}